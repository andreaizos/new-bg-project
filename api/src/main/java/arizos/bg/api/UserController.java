package arizos.bg.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@Autowired
	UsersService uservice;
	@Autowired
	UsersRepository rep;

	@GetMapping(path = "/api/v1/users")
	public List<Users> getAll() {
		return uservice.findAll();
	}

	@GetMapping(path = "/api/v1/users/{id}")
	public Users getuser(@PathVariable("id") int id) {
		return uservice.getUser(id);
	}

	@PostMapping(path = "/api/v1/users", produces = "application/json", consumes = "application/json")
	public Users createUser(@RequestBody Users user) {
		return uservice.saveUser(user);

	}

	@PutMapping(path = "/api/v1/users/{id}", produces = "application/json", consumes = "application/json")
	public Users updateUser(@RequestBody Users name, @PathVariable("id") int id) {
		return uservice.updateUser(name, id);

	}

	@DeleteMapping(path = "/api/v1/users/{id}")
	public String updateUser(@PathVariable("id") int id) {
		Users user = uservice.getUser(id);
		uservice.deleteUser(user);
		return "El usuario con el ID " + id + " fue eliminado";

	}

}
