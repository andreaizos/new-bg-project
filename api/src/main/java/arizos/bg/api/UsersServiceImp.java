package arizos.bg.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersServiceImp implements UsersService {

	@Autowired
	UsersRepository rep;

	@Override
	public List<Users> findAll() {
		return (List<Users>) rep.getall();
	}

	@Override
	public Users getUser(int id) {

		return rep.findUser(id);
	}

	@Override
	public Users saveUser(Users user) {
		return rep.save(user);
	}

	@Override
	public Users updateUser(Users name, int id) {
		Users userinfo = rep.findUser(id);
		userinfo.setName(name.getName());
		return rep.save(userinfo);
	}

	@Override
	public void deleteUser(Users user) {
		rep.delete(user);
	}

}
