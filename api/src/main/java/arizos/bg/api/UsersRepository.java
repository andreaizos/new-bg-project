package arizos.bg.api;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepository extends CrudRepository<Users, Integer> {

	@Query(value = "select * from users", nativeQuery = true)
	List<Users> getall();

	@Query(value = "SELECT * FROM Users WHERE Id = ?1", nativeQuery = true)
	Users findUser(int id);

}
