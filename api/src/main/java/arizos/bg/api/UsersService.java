package arizos.bg.api;

import java.util.List;

public interface UsersService {

	List<Users> findAll();

	Users getUser(int id);

	Users saveUser(Users user);

	Users updateUser(Users name, int id);

	void deleteUser(Users user);

}
