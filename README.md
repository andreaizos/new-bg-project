# REST API - Reto BG
####Contenido

[TOCM]


####Introducción
Este proyecto consiste en la creación de un API Rest, el cual contiene las siguientes resposabilidades:
- Debe responder un listado de usuarios al invocarlo de esta forma:
`GET http://localhost:8080/api/v1/users`
- Debe responder un solo usuario, al invocarlo de esta forma:
`GET http://localhost:8080/api/v1/users/{id}`
- Debe crear un nuevo usuario al invocarlo de esta forma:
`POST http://localhost:8080/api/v1/users`
- Debe modificar un usuario al invocarlo de esta forma:
`PUT http://localhost:8080/api/v1/users/{id}`
- Debe eliminar un usuario al invocarlo de esta forma:
`DELETE http://localhost:8080/api/v1/users/{id}`


**Construcción**
- Spring Boot (Java)
- Maven
- MySQL

####Instalación
En la pestaña `File`, importar proyecto como "Existing Maven Proyects".
Seleccionar la carpeta que contiene el proyecto `api`

####Directorio Elementos Trabajados 
+ **src/main/java**
	+ **arizos.bg.api**
    	+ **ApiApplication.java** - *Main Controller*
    	+ **UserController.java** - *Invocacion de Métodos*
    	+ **Users.java** - *Declaracion de la Tabla*
		+ **UsersRepository.java** - *Repositorio donde se encuentran los querys a ejecutar en la BD*
		+ **UsersService.java** - *Declaracion de Servicios*
		+ **UsersServiceImp.java ** - *Implementacion de los Servicios*
+ **src/main/resources**
    * **application.properties** - *Conexión de la base de datos*
    * **data.sql** - *Script para la creacion de BD y tabla con valores*
+ **pom.xml** - *Adición de Dependencias*


####Preparacion de la BD
En la ruta `src/main/resources` va a estar le archivo `application.properties` donde se contienen los datos para la conexión de la base de datos.
Actualmente la configuración es la siguiente:

    spring.datasource.type=com.zaxxer.hikari.HikariDataSource
    spring.datasource.url=jdbc:mysql://localhost:3306/reto_bg
    spring.datasource.username=root
    spring.datasource.password=`

##METODOS Y EJECUCIÓN
*Los ejemplos mostrados fueron ejecutados en POSTMAN*
###GET - Call All Users 
> Busca e imprime todos los usuarios almacenados en la BD

`GET http://localhost:8080/api/v1/users`
#### Ejemplo de Ejecucion
Solicitud
```json
curl --location --request GET 'http://localhost:8080/api/v1/users'
```
Respuesta
```json
[
  {
    "name": "arizos",
    "id": 1
  },
  {
    "name": "pa02",
    "id": 2
  },
  {
    "name": "pa04",
    "id": 4
  },
  {
    "name": "pa05",
	"id":5
	}
]
```
###GET - Call User By Id 
> Busca el id que se le proporciona en la BD y lo muestra.

`GET http://localhost:8080/api/v1/users/{id}`
#### Ejemplo de Ejecucion
Solicitud
```json
curl --location --request GET 'http://localhost:8080/api/v1/users/1'
```
Respuesta
```json 
{
  "name": "arizos",
  "id": 1
}
```
###POST - Create New User 
> Crea un nuevo usuario en la BD al ingresar un valor tipo string.

`POST http://localhost:8080/api/v1/users/`
#### Ejemplo de Ejecucion
Ingreso de valor en formato JSON
```json
{
    "name": string
}
```

BODY raw
```json
{
    "name": "pa013"
}
```
Solicitud
```json
curl --location --request POST 'http://localhost:8080/api/v1/users' \
--data-raw '{
    "name": "pa013"
}'
```
Respuesta
```json
{
  "name": "pa013",
  "id": 15
}
```
###PUT - Modify User By Id 
> Al ingresar un id específico en el url, pide modificar el nombre de usuario, donde se dee ingresar un valor tipo string.

`PUT http://localhost:8080/api/v1/users/{id}/`
#### Ejemplo de Ejecucion
Ingreso de valor en formato JSON
```json
{ 
   "name" : string
}
```
BODY raw
```json
{ 
   "name" : "arizos"
}
```
Solicitud
```json
curl --location --request PUT 'http://localhost:8080/api/v1/users/1' \
--data-raw '{ 
   "name" : "arizos"
}'
```
Respuesta
```json
{
  "name": "arizos",
  "id": 1
}
```
###DELETE- Delete User By Id  
> Al ingresar un id específico en el url, se elimina un usuario y manda mensaje de éxito.

`DELETE http://localhost:8080/api/v1/users/{id}/`
#### Ejemplo de Ejecucion
Solicitud
```json
curl --location --request DELETE 'http://localhost:8080/api/v1/users/3' \
--header 'Content-Type: application/json'
```
Respuesta
```json 
El usuario con el ID 3 fue eliminado
```
:fa-linkedin-square:  https://www.linkedin.com/in/andrea-r-izos/

###End